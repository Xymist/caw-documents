**Repository Notes**

This repository is intended to provide a version-controlled collection of the documents generated in the course of the ASTF project. Each major folder contains an area for deprecated file versions (i.e. pre-VCS historic records). Going forward these will no longer be generated, and version control will be conducted on this end via the integrated commit tracking capability.

If you wish to edit any of these, please remember to 'commit' and then submit a pull request so that we can keep a correct edit history.

*Jamie Duerden*
