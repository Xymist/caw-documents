**VoIP FAQ**

This is intended to provide a 'ready reference' for the VoIP system, including the frequently asked questions and issue which have arisen in the time it has been installed.

It supplements the crib sheet, which you should already have.

**Administrative Issues**

 - I need to know how many calls X is making
 - I need to know who is online
 - I need to remove someone from the system
 - We have a new volunteer who needs a login
 - I need statistics on (something)

 For all these and similar issues, please refer to Michele, Sally or Fiona.

**Call History**

 - On the white strip with buttons on the far right of the phone there is a button with a symbol resembling a clock running backwards. This is your call history.
  - A call which came in and was answered is represented by a handset with a left-pointing arrow.
  - A call which came in and was not answered is represented by a handset with a U-shaped arrow.
  - A call which you made and was answered is represented by a handset with a right-pointing arrow.
  - A call which you made and was not answered is represented by a handset with a cross.
 - You may page through this with the 'up' and 'down' arrow buttons under the screen, and select a number to call back with 'tick'.

**Call Problems**

 - My phone doesn't ring!

    There are three possible reasons for this: either you are on 'Do Not Disturb', you have been assigned a custom (silent) ringtune by mistake, or you have not logged in properly.

    Check your login status. If that is correct, check that there is no 'Do Not Disturb' symbol on the screen. This looks like a handset with a 'no entry' sign in the bottom right hand corner of the green screen.

    If there is, press the 'Do Not Disturb' button; this is the topmost of the vertical column of grey buttons, and is accompanied by a symbol representing a handset with a bar through it.

    If both of these are correct, please contact Michele to have the empty entry removed from your 'tunes' tab on the management system.

 - My call quality drops occasionally

    This can happen, as with any phone system, and is often due to issues with BT's connection rather than us. If it persists in a particular location, please tell Michele, Sally or Fiona the handset, day, time and office in which the issue occurred, and a report will be made to our suppliers.

 - I've plugged in the headset, but the person on the other end can't hear me (/vice-versa)

    Please ensure that the headset is plugged in all the way, and into the socket on the *back* of the phone. The headset does not replace the handset, there is a separate headset socket. The plug requires some firmness when connecting it to the phone.
    If *they* can't hear *you*, ensure that your 'mute' button is not pressed. This is the button on the bottom grey row with a crossed-out microphone.

**Dialling**

 - The phones work essentially as you would expect in this respect; just pick up the handset and dial the extension or external number of your target person. You will always need the area code, even for a local call, as the connection is made via the internet and could enter the 'standard' phone system from anywhere in the UK.

 - By default, the number from which you are calling is withheld. If you are dealing with someone who will not pick up calls from a withheld number, you can dial '9' before the number you need to call; this will display the number of your office.

 - If you are calling someone back from AdviceLine, please continue to use the AdviceLine call back function.

**Directory**

 - We have a directory function on the phones which provides us with a simple means of finding the numbers (both internal and external) of management staff, ASSs, Trustees and a number of other organisations within Waverley and central Government (for example, HMRC). You cannot search for volunteers in this way.

 - On the bottom row of grey buttons, there is one accompanied by a symbol resembling a book. This is the directory. Press this and the phone will request that you enter a name. We have organised the internal ones by first name; letters are entered by pressing the number they appear above **once**. For example, if you want to enter the letter C, you simply press '2' once, even though it is the third letter on the number 2 button.

 - Once you see the person or organisation you need on the screen, scroll to them with the arrow keys under the screen and press 'tick' to be shown their extension or number. Pressing 'tick' again will dial that number.

**Hold**

 - How do I place someone on hold?
  - The third button up on the white column of buttons on your phone has a double-bar similar to the 'pause' symbol. This is the Hold button. While it is pressed, you will not hear the client, and they will hear intermittent beeps or hold music depending on which line you are talking to them on.
  - While the Transfer process is in progress whoever you are not talking to will be on hold.
  - There is a box called 'Park', accessible from the (P) button on your handset (this is on the far right white column). Pressing this while on the call will add the caller to the first available Park slot. Refer to the Park section for further details.


 - How do I take someone off hold?
  - Hang up the phone, or
  - Press the Hold button again, or
  - Complete the transfer, or
  - Enter the Park menu from the Park button, and select their call with 'tick'.

**International and Premium Calls**

Numbers beginning 09, 070, and +00 are by default unavailable. If you need to make one of these calls on behalf of a client, you can dial \*99 before the number, which will allow the call to go through. Please be aware that unlike internal, local, national and mobile calls these are *not* free under our contract with the VoIP supplier, and will incur significant charges to the Bureau.

**Logging In**

The login procedure is as follows:
 - *Do not* lift the handset.
 - Enter your user number (e.g. 1234)
 - Enter a star (\*)
 - Enter your pin (e.g. 1234 - This will always be the same as your user number)
 - You will now have eight digits with a star in the middle, e.g 1234\*1234
 - Press 'tick' on the furthest right button of the four immediately under the green screen on the phone.

If the 'Logging In' message displays for more than about ten seconds, the phone needs rebooting.
 - Ensure your PC is OFF, or else it will freeze. If it freezes, it will not recover by itself; hold down the power button on the PC itself until it fully turns off, then start it up again.
 - Remove the ethernet cable from the port labelled 'LAN' (carefully, making sure to squeeze the locking tab so you don't damage it; it should not require force) and replace it.
 - Wait for the phone to boot, then try logging in again.

**Logging Out**

 - This will happen automatically, eight hours after the phone is last used. However, it is good practice to log out when you leave the office.

 - The topmost left button on the phone has a crossed spanner and screwdriver. Pressing this will give you the 'tools' menu, with 'Log out' already selected. Press 'tick' to be logged out and returned to the main screen.

**Park**

  - This is a function whereby calls may be transferred across an office (not across the District) while placing them on hold in the meantime. There are four park slots available to each office, and they are shared - a call stored in one slot will be available to whomever in the office chooses to take it. The person in the Park slot is unaware of this, and is simply 'on hold' from their perspective.

**Transfers and Conference**

 - To make a transfer or set up a conference call, first press the Transfer button *while* you are talking to the first person you wish to include. This is the bottom-rightmost grey button, with an image of a handset and an upwards-pointing arrow. Upon pressing this button, the person you're currently talking to will be placed on hold and you will be presented with a dial tone.

 - Dial the extension number or external number you wish to make the transfer to, or add to your conference. If you don't know it, it is safe at this time to use the Directory as detailed above.

  - If you wish to make a transfer, once your second connection has picked up you may simply put your handset down. Do not do this before they have acknowledged the call, or you run the risk of cutting off both people.

  - If you wish to create a conference call, instead of placing the handset back in its cradle, you can press the conference button. This is on the vertical column of grey buttons, with a symbol representing three stacked handsets. This will open both connections, so that all three participants can hear each other. Conference calls created this way can only include three people at most.

 - If, in the course of this process, you need to switch back and forth between your two connections, press the Switch button. This is again on the vertical column of grey buttons, and has a symbol representing two handsets with two opposing arrows between them.


**Voicemail**

 - How do I find my voicemail?
    The voicemail server is available by dialling 1572
    The voicemail for which you have notifications is available by pressing the button to the right of the picture of an envelope, on the right hand side of your phone.

 - What is my access code? / What is my account number?
    These are both the same as the extension number of the voicemail box which you are accessing.

 - What are the voicemail box extension numbers?
    Each office has two voicemail boxes to which you have access. (If you are management and have a personal voicemail box, you will have that available as well)
    These are the 'Appointments' line, which always goes directly to voicemail, and the 'Bureau' line, which replaces what used to be the ex-directory and casework .
    Their extensions are available in the directory, at the bottom of the list.

 - How do I delete a voicemail?
    Press '3' *while it is playing*
    OR
    If you have accessed it via the green screen on your phone, pressing the 'down' arrow under the screen will scroll through options until you see 'Delete'.
    A voice will ask you for your access code to confirm you are authorised to delete messages from that box; this is the extension number of the line the message was for.

 - **Example:** You want to listen to the voicemails for the Godalming Appointments Line.
  - Dial 1572. A voice will ask you to enter your account number, followed by #.
  - Look up the Godalming Appointments Line; in this particular case that's extension 1200.
  - Enter '1200#'. A voice will ask you to enter your access code, followed by #.
  - Enter '1200#' again. The server will begin to play the most recent messages for you.

**What are all those other buttons for?**

 - The button with the quarter-circles (((@))) is the speakerphone button. This is also how you pick up a call when the phone rings and you have a headset plugged in.
 - The button on the vertical grey column with the symbol of two cogs is the Settings button. We're not using any of the functions on here.
 - The red dot is the record button; we do not have this functionality enabled.
 - The two buttons with 'speech bubbles' are for personal voicemail. We do not have this functionality enabled.
 - The two buttons at the top of the white strip on the right (with the ascending and descending vertical bars) are the volume controls. These apply to whatever function you are using at the time:
   - If you're not on the phone (i.e. 'nothing'), it controls the **ring volume**
   - If you're on the phone using the handset, it controls the **handset volume**
   - If you're on the phone using the headset, it controls the **headset volume**
   - If you're on the phone using the speaker, it controls the **speakerphone volume**
