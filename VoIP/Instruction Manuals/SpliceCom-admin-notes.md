**Notes for the use of the SpliceCom admin panel**

All of the below can only be done from inside one of the office networks.

You should be aware that:

 - 10.33.228.250 is the Godalming (i.e. main) PBX. ...225.250 is Haslemere, and ...227.250 is Farnham. You should never need to use either of these, as all changes are propagated outwards from the main PBX to the two subsidiaries.
 - Further to this, the system is dependant on the Godalming PBX. Should Godalming go down, the phones will continue to work internally, but will not function for external calls either in or out. This would be an exceptional circumstance, as both the Godalming broadband lines would need to fail at the same time. It might occur in the event of a protracted powercut which the UPS is unable to cover, however.
 - In the event of a systems failure, or a required change which you are either not authorised or not confident to make, we have a support contract with M12. Call 03444 777 600 to reach their support team, who will take care of such issues on an as-needed, 9-5 Monday to Friday basis.

**Manager administration interface**

Log in to the system by opening up your browser and accessing 10.33.228.250/manager/

The user name is *CAB* and the password is *CABsplice*

As an end user, you have a restricted login to this area, but should be able to make most day-to-day changes with minimal difficulty.

This will cover:

 - Adding, altering or deleting a user [^1]
 - Adding, altering or deleting a group, including order of call flow [^2]
 - Adding, altering or deleting a department [^3]

There are other functions available, but the use case is small and the potential damage is significant. Refer to Michele or M12 support (03444 777 600) for more complex alterations to the system.


[^1]! **Users**

A User is an individual on the system, either a human being or a shared account. It has a name, a description, some configuration details and an extension number. We have 200 user licences; all of these are already assigned a name and extension number. Therefore, do not click 'Add' on the user list. Never change an extension number; duplicates or missing routes within the system will cause call failures and are exceedingly hard to track down.

On the left of the screen, you will see that there is a 'Users' tab. Click this to be presented with the list of users, in alphabetical order. Volunteers are all under 'V', as their names are all 'Volunteer ####'. Searching by description is unfortunately not possible, so searching for a volunteer by their name is not feasible.

To add a user, find the first 'Volunteer ####' the description of which is 'Volunteer ####'; these are the unoccupied slots.

If the new user is a volunteer, change the description to their name.
If the new user is paid staff, change the name to their name and the description to their role (e.g. ASS)
The rest of this page you can ignore. Scroll to the bottom and click 'Apply'.

Switch to the 'Telephony' tab at the top of the screen.
'Forward on busy' and 'Forward on no answer' should be identical, and usually 'Internal&External'. Their respective settings should also be identical, and usually will be the main extension (1000, 2000, 3000 or 4000) of the office to which they are assigned. Further down, there is a 'No answer time' box; this by default is 9 seconds, (three ring cycles). You may customise it as necessary within the bounds set by the department ring cycle timings. The rest of this page you can ignore. Scroll to the bottom and click 'Apply'.

Next move to the 'Voicemail' tab. If the user requires access to voicemail boxes (usually the Bureau and Appointments Departments for the office(s) to which they are assigned) click on the first 'Department Voicemail' link. It may ask whether you wish to leave the form; select 'OK'. Click the first relevant department. Repeat this procedure with the second line until all departments have been assigned as necessary. The rest of this page you can ignore. Scroll to the bottom and click 'Apply'. If this is your final edit to this user, click 'Update' to be returned to the 'Users' list.

If (and only if) the user is paid staff and has sufficient reason for a personal voicemail box, you may have requested a voicemail licence for them. We have a finite supply of these, and they are not inexpensive. If one has been assigned, select the 'Licences' tab at the top. Check just the box by 'Message Box Licence'. Then return to the 'Voicemail' tab, and select 'Voicemail Enabled'.
The rest of this page you can ignore. Scroll to the bottom and click 'Update'.

To edit a user, find them either as a volunteer or by their first name for paid staff. You can search "volunteer x" to get the list of volunteers for a given site, in the search box in the upper left. Follow the procedure as for adding a user, changing only the relevant fields.

To remove a user, DO NOT DELETE THE USER RECORD.
Navigate to the user's page as before. Edit their name and description to 'Volunteer ####'. Apply, move to the 'Telephony' tab, alter the forwarding settings to 'None' and blank. Apply, move to the 'Voicemail' tab, click each assigned 'Department Voicemail' link, scroll to the bottom of the page and click 'clear'. Apply. If they had a voicemail licence assigned, move to the 'Licences' tab and uncheck the 'Message Box Licence'. Update to return to the user list.


[^2]! **Groups**

A Group is a list of users who are called together, either en masse or consecutively, in response to a call routing for a Department. This includes sets such as 'District Management', and 'Farnham ASSs'.

On the left of the screen, you will see that there is a 'Groups' tab. This lists all the groups currently in existence on the system.

To add a group, scroll to the bottom and click 'Add'. Give the new group a name and description, then click 'Apply'.
Then select 'Add User Member'. You will be presented with the user list. Select or search for the user you wish to add, and click on their name. They will be added to the group and you will be returned to the group's page. Repeat until all desired users have been added. These changes will be saved automatically.

To edit a group, click on the group name. You may edit the name or description, then click 'Apply' if you have further changes to make, or 'Update' if you have no further changes.

To add a user to an existing group, use the same method as when creating a group.

To edit or remove a user from a group, or to change the order, click the NUMBER next to the user's name. Do not click the users name itself.

You can alter the call order by changing 'order number'. There may not be duplicates, fractional numbers or negative numbers. The LARGEST number will ring first if the group is set to ring in a rotary fashion, as with ASSs and Management.
To change the user, click the 'Member' link, and select the desired user from the user list.
To remove the user, click 'Delete', and confirm when asked.


[^3]! **Departments**

A Department is a group of Groups. They control the call flow throughout the organisation, and have their own extension numbers. A Group is useless without a Department, as Groups cannot have an extension.

On the left of the screen, you will see that there is a 'Departments' tab. This lists all the Departments currently in existence on the system.

To add a Department, click 'Add'. Give the new Department a useful name, and a description if you wish. Assign it an UNUSED extension (Telephone Number), ideally greater than 5000. It must be four digits; we are reserving the 1xxx, 2xxx, 3xxx and 4xxx extensions for users and office-internal groups.

Out Of Hours Mode should be 'Timed'. Click on 'In Hours Time Plan'; select the closest of the four available. DO NOT USE OR EDIT the 'Standard' time plan.

Assign 'Current Home' similarly. Cranleigh Departments are homed at the Haslemere PBX. You can ignore the rest of this tab and click 'Apply'.

Move to the 'Distribution' tab. Select the Groups you wish to receive calls to that Department, in order of call flow. Choose your desired Distribution Mode; this is never 'Sequential' or 'Manual'.
'Rotary' will call the highest-numbered User in that group first, and then on down the list.
'All' will ring every phone in that group at once.
Ensure 'Skip On No Targets' is checked in both places, then click 'Apply'.

Move to the 'Telephony' tab.
'Max Ring Time Before Alternate Distribution' should be 15.
'Max Ring Time Before Alternate Distribution 2' should be 10.
'Max No Answer Time Before Voicemail' should be 35 if there are three groups in the Department, or 25 otherwise.
You can ignore the remainder of this page; click 'Apply', unless this is your final change to the Department, in which case click 'Update'.

Depending on the nature of the Department, you may have requested a voicemail licence for it. We have a finite supply of these, and they are not inexpensive. If one has been assigned, select the 'Licences' tab at the top. Check just the box by 'Message Box Licence'. Then return to the 'Voicemail' tab, and select 'Voicemail Enabled'.
The rest of this page you can ignore. Scroll to the bottom and click 'Update'.


**Vision reporting Pack**

Log in to the system by opening up your browser and accessing 10.33.228.250/vision/

The user name is *admin* and the password is *splicecom 123*

The Vision Reporting Software is intended for monitoring call use, dropped calls, communications load and so on. It is read-only, so you cannot affect the system through this interface;

The Green reports are capacity. We have 30 SIP channels available to us; that means 30 inbound or outbound calls in total may occur across the district at once. These are clearly labelled, but not immensely useful as we are unlikely to reach this number.

The Orange reports are essentially useless; we are not on a paid call plan, and therefore the figures are wildly inaccurate as we are not charged for any calls other than premium rate lines.

The Blue reports are our call logs. We can identify calls which were successful, which were abandoned, time spent on the phone, incoming:outgoing call ratio, and various other measurements. You can identify these things for Users or Departments; all reports are clearly labelled.

To find the call log for an individual user (including any of the touchscreen terminals once those are phone-enabled), go to the report labelled 'Overview by User'. Type the name of the person, terminal or department you want the information for into the search box, select your time period in the dropdown, and click 'generate report'. You will see an overview with bar graph. For detailed call logs, click the name; you will be presented with a list of all calls made in that time period, including the names of internal callers, the duration and connected time of the call, and the number connected to. This last will need to be manually matched with external individuals or organisations, except where they have been made 'contacts' on the server.
