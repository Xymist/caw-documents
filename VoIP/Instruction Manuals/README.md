These PDFs are generated automatically from the Markdown files in this folder. The latter should be considered the canonical document; the former are produced for distribution reasons only.
