**Config details for Ekiga SIP client**

**Account Settings:**
*Name:* Name of kiosk, e.g. gp-godalming
*Registrar:* 10.33.228.250, OR the external IP of the Godalming firewall if not using VPN.
*User:* User #, e.g. 1061
*Authentication User:* Ditto
*Password:* Ditto
*Timeout:* 3600

**Preferences**
 - **Protocols**
  - **SIP Settings**
   - *Outbound Proxy:* 10.33.228.250, OR the external IP of the Godalming firewall if not using VPN.
   - *Send DTMF as:* RFC2833
  - **Audio**
   - *Codecs:* ONLY PCMA (Deselect all others)

**Call Dialling Format:**
sip:{dialled-number}@10.33.228.250
